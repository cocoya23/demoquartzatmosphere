package mx.com.drdice.demoquartzweb.bean;

import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import mx.com.drdice.demoquartzweb.job.HelloJob;
import static org.quartz.CronScheduleBuilder.cronSchedule;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.Scheduler;

import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerListener;
import org.quartz.impl.StdSchedulerFactory;
import static org.quartz.JobBuilder.*;
import static org.quartz.TriggerBuilder.*;
import static org.quartz.SimpleScheduleBuilder.*;

@ManagedBean(name = "job")
@ApplicationScoped
public class JobBean {

    private String timeToNext;
    private String status;
    private int progeso;
    private SchedulerFactory sf;
    private Scheduler sched;
    private JobDetail job;
    private Trigger trigger;
    public static final SimpleDateFormat FORMAT = new SimpleDateFormat("dd/MM/yyy hh:mm:ss");

    public JobBean() {
        try {

            sf = new StdSchedulerFactory();
            sched = sf.getScheduler();

            job = newJob(HelloJob.class)
                    .withIdentity("job1", "group1")
                    .build();

            trigger = newTrigger()
                    .withIdentity("trigger1", "group1")
                    .startNow()
                    .withSchedule(simpleSchedule()
                            .withIntervalInSeconds(15).
                            repeatForever())
                    .build();

            sched.scheduleJob(job, trigger);

            sched.start();

            checaTiempo();

        } catch (SchedulerException ex) {
            Logger.getLogger(JobBean.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void checaTiempo() {

        //status = sched.getTriggerState(trigger.getKey()).toString();
        status = "Desconocido";
        try {
            timeToNext = FORMAT.format(sched.getTrigger(trigger.getKey()).getNextFireTime());
        } catch (SchedulerException ex) {
            Logger.getLogger(JobBean.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }

    }

    public String getTimeToNext() {
        return timeToNext;
    }

    public void setTimeToNext(String timeToNext) {
        this.timeToNext = timeToNext;
    }

    public int getProgeso() {
        return progeso;
    }

    public void setProgeso(int progeso) {
        this.progeso = progeso;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
