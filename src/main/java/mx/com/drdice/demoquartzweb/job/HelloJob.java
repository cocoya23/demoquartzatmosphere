package mx.com.drdice.demoquartzweb.job;

import mx.com.drdice.demoquartzweb.bean.JobBean;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class HelloJob implements Job{

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        System.out.println("=====================================");
        System.out.println("Hola mundo");
    }
    
}
